import numpy as np 
import matplotlib.pyplot as plt

img = plt.imread("road.jpg")
img = img[:,:,0].copy()

#a)
for i in range(427):
    for j in range(640):
        if img[i][j]<155:
            img[i][j] +=50 
        else:
            img[i][j] = 255

plt.imshow(img, cmap="gray")
plt.show()

#b)
cropped_image=img[100:300,200:400]
plt.imshow(cropped_image, cmap="gray")
plt.show()

#c)
rotated = np.rot90(img,3)

plt.imshow(rotated, cmap="gray")
plt.show()

#d)
mirrored = np.fliplr(img)

plt.imshow(mirrored, cmap="gray")
plt.show()