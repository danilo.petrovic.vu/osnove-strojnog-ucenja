import numpy as np
import matplotlib.pyplot as plt

x1 = np.array([1,3])
y1 = np.array([1,1])

x2 = np.array([3,3])
y2 = np.array([1,2])

x3 = np.array([3,2])
y3 = np.array([2,2])

x4 = np.array([2,1])
y4 = np.array([2,1])

plt.plot(x1,y1,'r',linewidth=2,marker='.', markersize=10)
plt.plot(x2,y2,'g',linewidth=2,marker='.', markersize=10)
plt.plot(x3,y3,'b',linewidth=2,marker='.', markersize=10)
plt.plot(x4,y4,'y',linewidth=2,marker='.', markersize=10)
plt.axis([0,4,0,4])

plt.show()