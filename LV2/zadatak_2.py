import numpy as np 
import matplotlib.pyplot as plt

x = np.loadtxt("data.csv", delimiter=",", dtype=str)
x = np.delete(x,0,0)

data = x.astype(np.float64) 

#a)
print("Izvrseno je mjerenje na", len(data), "osoba.")

#b)
plt.scatter(data[:,1],data[:,2])
plt.show()

#c)

x=data[::50,1]
y=data[::50,2]
plt.scatter(x,y)
plt.show()

#d)
heights=data[:,1]

print("Maksimalna vrijednost:", np.max(heights))
print("Minimalna vrijednost:", np.min(heights))
print("Srednja vrijednost:", np.mean(heights))
print("")
#e)
males=data[data[:,0]==1]
females=data[data[:,0]==0]

print("Maksimalna vrijednost(muskarci):", np.max(males[:,1]))
print("Minimalna vrijednost(muskarci):", np.min(males[:,1]))
print("Srednja vrijednost(muskarci):", np.mean(males[:,1]))
print("")
print("Maksimalna vrijednost(zene):", np.max(females[:,1]))
print("Minimalna vrijednost(zene):", np.min(females[:,1]))
print("Srednja vrijednost(zene):", np.mean(females[:,1]))