import numpy as np 
import matplotlib.pyplot as plt

black = np.zeros((50,50))
white = np.ones((50,50))

first_row = np.hstack((black,white))
last_row = np.hstack((white,black))

image = np.vstack((first_row,last_row))

plt.imshow(image, cmap="gray")
plt.show()