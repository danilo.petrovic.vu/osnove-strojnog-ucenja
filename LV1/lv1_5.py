fhand=open("SMSSpamCollection.txt")

spam_counter=0
ham_counter=0
spamWords=0
hamWords = 0
exclamationCounter = 0

for line in fhand:
    line=line.rstrip()
    words=line.split()
    if(line.startswith("spam")):
        spam_counter+=1
        spamWords+=len(words)
        if(line.endswith("!")):
            exclamationCounter+=1
    else:
        ham_counter+=1
        hamWords+=len(words)

print("Prosjecan broj rijeci u spam:", spamWords/spam_counter)
print("Prosjecan broj rijeci u ham:", hamWords/ham_counter)
print("Broj 'spam' sto zavrsava sa !:", exclamationCounter)