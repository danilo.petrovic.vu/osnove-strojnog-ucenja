def getCategory(x):
    if x<0.6:
        return "F"
    elif x<0.7:
        return "D"
    elif x<0.8:
        return "C"
    elif x<0.9:
        return "B"
    else:
        return "A"


try:
    x=float(input("Unesi ocjenu:"))
    if 0<=x<=1:
        print(getCategory(x))
    else:
        print("Unijeli ste ocjenu izvan intervala!")
except:
    print("Niste unijeli broj")