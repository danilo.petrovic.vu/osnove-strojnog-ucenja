def calculateAverage(numbers):
    sum=0

    for number in numbers:
        sum+=number
    
    return sum/len(numbers)

numbers=[]

while True:
    x=input("Unesite broj:")
    if x == "done":
        break
    else:
        try:
            numbers.append(float(x))
        except:
            print("Niste unijeli broj")

print("Korisnik je unio", len(numbers), "brojeva")
print("Srednja vrijednost:", calculateAverage(numbers))
print("Minimalna vrijednost:", min(numbers))
print("Maksimalna vrijednost:",max(numbers))
    