import pandas as pd
import matplotlib.pyplot as plt 

data = pd.read_csv('data_C02_emission.csv')

#a)
plt.figure()
data['CO2 Emissions (g/km)'].plot (kind='hist', bins = 20)
plt.show()

#b)
for col in data.select_dtypes(include='object'):
    data[col] = data[col].astype('category')
    
data.plot.scatter(x='Fuel Consumption City (L/100km)', y='CO2 Emissions (g/km)',
c='Fuel Type', cmap ="hot", s=50)
plt.show()

#c)
data.boxplot(column =['Fuel Consumption Hwy (L/100km)'], by='Fuel Type')
plt.show()

#d)

#e)
data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean().plot(kind='bar')
plt.show()