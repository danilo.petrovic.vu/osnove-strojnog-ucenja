import numpy as np 
import pandas as pd

data = pd.read_csv("data_C02_emission.csv")

#a)
print("Broj mjerenja:",len(data))
print(data.info())
print("Broj izostalih vrijednosti:", data.isnull().sum())
print("Broj dupliciranih vrijednosti:", data.duplicated().sum())
data.dropna(axis = 0)
data.drop_duplicates()
for col in data.select_dtypes(include='object'):
    data[col] = data[col].astype('category')
print(data.info())


#b)
print("Najmanji potrosaci:\n",data.sort_values("Fuel Consumption City (L/100km)", ascending=True)[["Make","Model","Fuel Consumption City (L/100km)"]].head(3))
print("Najveci potrosaci:\n",data.sort_values("Fuel Consumption City (L/100km)", ascending=False)[["Make","Model","Fuel Consumption City (L/100km)"]].head(3))

#c)
print("Broj vozila s velicinom motora 2.5-3.5L:", len(data[(data["Engine Size (L)"] >2.5) & (data["Engine Size (L)"] <3.5)]))
print("Prosjecna C02 emisija tih vozila:", data[(data["Engine Size (L)"] >2.5) & (data["Engine Size (L)"] <3.5)]["CO2 Emissions (g/km)"].mean())

#d)
print("Broj audija:",len(data[data["Make"] == "Audi"]))
print("Prosjecna potrosnja audi sa 4 cilindra:", data[(data["Make"] == "Audi") & (data["Cylinders"] == 4)])

#e)
print("Broj automobila po cilindrima:\n",data.groupby(by='Cylinders')['Cylinders'].count())
print("CO2 po cilindrima:\n",data.groupby(by='Cylinders')['CO2 Emissions (g/km)'].mean())

#f)
print("Prosjecna potrosnja dizela:", data[data["Fuel Type"] =="D"]["Fuel Consumption City (L/100km)"].mean())
print("Prosjecna potrosnja benzinca:", data[data["Fuel Type"] =="X"]["Fuel Consumption City (L/100km)"].median())

print("Medijalna potrosnja dizela:", data[data["Fuel Type"] =="D"]["Fuel Consumption City (L/100km)"].mean())
print("Medijalna potrosnja benzinca:", data[data["Fuel Type"] =="X"]["Fuel Consumption City (L/100km)"].median())

#g)
#print("Dizel s 4 cilindra s najvecom potrosnjom:",data[(data["Fuel Type"] == "D") & (data["Cylinders"] == 4)].max()[["Make", "Model", "Vehicle Class"]])

#h)
#print("Broj vozila s mjenjacem:",len(data["Transmission"].starts"M"))

#i)
print(data.corr(numeric_only=True))